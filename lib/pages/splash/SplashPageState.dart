/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'dart:async';

import 'package:artifact/pages/home/HomePage.dart';
import 'package:artifact/utils/StyleConstants.dart';
import 'package:artifact/widgets/Heading1Text.dart';
import 'package:artifact/widgets/ParagraphText.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'SplashPage.dart';

class SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return new Scaffold(
        body: new Center(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Heading1Text("Global Stratgic"),
                  new Heading1Text("Threat and Design"),
                  new Padding(padding: new EdgeInsets.all(StyleConstants.MATERIAL_PADDING))
                ],
              ),
              new Column(
                children: [ new ParagraphText("3rd march - 5th March Serena Hotel, Islamabad"),
                  new Padding(padding: new EdgeInsets.all(StyleConstants.MATERIAL_PADDING))],
              ),
              new Column(
                children: [ new CircularProgressIndicator(),
                  new Padding(padding: new EdgeInsets.all(StyleConstants.MATERIAL_PADDING_SCREEN))],
              ),

              new Column(
                children: [ new ParagraphText("By"),
                  new Padding(padding: new EdgeInsets.all(StyleConstants.MATERIAL_PADDING))],
              ),
              new Column(
                children: [
                  new Heading1Text("CASS"),
                  new ParagraphText("CENTER for AEROSPACE & SECURITY STUDIES"),
                  new Padding(padding: new EdgeInsets.all(StyleConstants.MATERIAL_PADDING))],
              )
            ],
          ),
        ));
  }

  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomePage()));
  }
}
