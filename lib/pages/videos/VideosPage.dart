/*
// Created by UMAIR KHALID on 1/14/2020.
*/

import 'package:artifact/models/Seminar.dart';
import 'package:artifact/pages/videos/VideosPageState.dart';
import 'package:flutter/material.dart';

class VideosPage extends StatefulWidget {

  final List<Seminar> seminars = [
    new Seminar(
        "Lahore eat", "Asrar", "https://www.youtube.com/watch?v=JtYwqp3OzMk"),
    new Seminar(
        "Gujranwala eat", "Mark Wiens", "https://www.youtube.com/watch?v=N7kvo94PKN4"),
    new Seminar(
        "Karachi eat", "Geo News", "https://www.youtube.com/watch?v=UpNjdGjcDyk"),
    new Seminar(
        "Multan eat", "GNN", "https://www.youtube.com/watch?v=zW9clge099Y")
  ];

  @override
  State<StatefulWidget> createState() {
    return new VideosPageState();
  }
}
