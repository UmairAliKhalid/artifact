/*
// Created by UMAIR KHALID on 1/14/2020.
*/

import 'package:artifact/pages/videos/VideosPage.dart';
import 'package:flutter/material.dart';

class VideosPageState extends State<VideosPage> {
  List<TableRow> tableRows = [];

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([]);

    return new Scaffold(
        body: new Center(
            child: new Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: getTableRows(),
    )));
  }

  List<TableRow> getTableRows() {
    tableRows.clear();

    for (int i = 0; i < widget.seminars.length; i + 2) {
      if (i > 0) {
        tableRows.add(new TableRow(children: [Text("Cell1"), Text("Cell2")]));
      }
    }

    return tableRows;
  }
}
