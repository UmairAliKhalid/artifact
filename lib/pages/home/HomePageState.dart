/*
// Created by UMAIR KHALID on 1/13/2020.
*/

import 'package:artifact/fragments/BusinessFragment.dart';
import 'package:artifact/fragments/HelpFragment.dart';
import 'package:artifact/fragments/HomeFragment.dart';
import 'package:artifact/fragments/PizzasFragment.dart';
import 'package:artifact/fragments/SchoolFragment.dart';
import 'package:artifact/pages/home/HomePage.dart';
import 'package:artifact/pages/programs/ProgramsPage.dart';
import 'package:artifact/pages/videos/VideosPage.dart';
import 'package:flutter/material.dart';

class HomePageState extends State<HomePage> {
  int selectedDrawerIndex = 0;
  int selectedBottomNavIndex = 0;


  var appBarTitleText = new Text("Home");

  getDrawerItemWidget(int leftMenuIndex, int bottomIndex, bool isFromLeftMenu) {
    if (isFromLeftMenu) {
      switch (leftMenuIndex) {
        case 0:
          return new VideosPage();
        case 1:
          return new PizzasFragment();
        case 2:
          return new HelpFragment();

        default:
          return new Text("Error");
      }
    } else {
      switch (bottomIndex) {
        case 0:
          return new ProgramsPage();
        case 1:
          return new BusinessFragment();
        case 2:
          return new SchoolFragment();

        default:
          return new Text("Error");
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    var drawerOptions = <Widget>[];
    var bottomNavOptions = <BottomNavigationBarItem>[];

    for (int i = 0; i < widget.drawerItems.length; i++) {
      var item = widget.drawerItems[i];
      drawerOptions.add(new ListTile(leading: new Icon(item.icon),
          title: new Text(item.title),
          selected: i == selectedDrawerIndex,
          onTap: () => onDrawerItemClick(i)));
    }

    for (int i = 0; i < widget.bottomNavItems.length; i++) {
      var item = widget.bottomNavItems[i];
      bottomNavOptions.add(new BottomNavigationBarItem(icon : new Icon(item.icon) , title: new Text(item.title)));
    }

    return new Scaffold(
      appBar: new AppBar(elevation: 10.5,
          title: appBarTitleText),

      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                accountName: new Text("Umair khalid"),
                accountEmail: new Text("umair.khalid@gmail.com")),
            new Column(children: drawerOptions)
          ],
        ),
      ),

      bottomNavigationBar: new BottomNavigationBar(
          items: bottomNavOptions,
          currentIndex: selectedBottomNavIndex,
          selectedItemColor: Colors.amber[800],
          onTap: onBottomNavItemClick),

      body: getDrawerItemWidget(selectedDrawerIndex, selectedBottomNavIndex, widget.isLeftMenuSelected),
    );
  }

  onDrawerItemClick(int i) {
    setState(() {
      selectedDrawerIndex = i;
      widget.isLeftMenuSelected = true;

      appBarTitleText = new Text(widget.drawerItems[selectedDrawerIndex].title);
      //*********Closing drawer*************//
      Navigator.of(context).pop();
      widget.log?.e("test left menu " + widget.isLeftMenuSelected.toString());
    });
  }

  onBottomNavItemClick(int i) {
    setState(() {
      selectedBottomNavIndex = i;
      widget.isLeftMenuSelected = false;
      appBarTitleText = new Text(widget.bottomNavItems[i].title);
      widget.log?.e("Bottom navigation " + widget.isLeftMenuSelected.toString());
    });
  }
}