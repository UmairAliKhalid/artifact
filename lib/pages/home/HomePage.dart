/*
// Created by UMAIR KHALID on 1/13/2020.
*/

import 'package:artifact/models/BottomNavItem.dart';
import 'package:artifact/models/DrawerItem.dart';
import 'package:artifact/pages/home/HomePageState.dart';
import 'package:artifact/utils/Log.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {

  var log = new Log();
  var isLeftMenuSelected = false;

  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  final drawerItems = [
    new DrawerItem("Media", Icons.perm_media),
    new DrawerItem("Pizzas", Icons.local_pizza),
    new DrawerItem("Help", Icons.info)
  ];

  final bottomNavItems = [
    new BottomNavItem("Home", Icons.home),
    new BottomNavItem("Business", Icons.business),
    new BottomNavItem("School", Icons.school)
  ];

  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}
