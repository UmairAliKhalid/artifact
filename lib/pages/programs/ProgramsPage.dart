/*
// Created by UMAIR KHALID on 1/10/2020.
*/

import 'package:artifact/models/ProgramsDetail.dart';
import 'package:artifact/pages/programs/ProgramsState.dart';
import 'package:flutter/widgets.dart';

class ProgramsPage extends StatefulWidget {

  final programsDetails = [
    new ProgramsDetail("Health care", "Session about health care importance"),
    new ProgramsDetail("Buisness Development", "Session about health care importance"),
    new ProgramsDetail("Lorem ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
        "At quanta conantur! Mundum hunc omnem oppidum esse nostrum! "
        "Incendi igitur eos, qui audiunt, vides. Quid nunc honeste dicit? "
        "Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? "
        "Rationis enim perfectio est virtus; Quid enim de amicitia statueris utilitatis causa expetenda vides. "
        "Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. "
        "Duo Reges: constructio interrete. "
        "Satisne ergo pudori consulat, si quis sine teste libidini pareat? At multis se probavit."),
  ];

  @override
  State<StatefulWidget> createState() {

    return new ProgramsState();
  }
}
