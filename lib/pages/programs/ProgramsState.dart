/*
// Created by UMAIR KHALID on 1/10/2020.
*/

import 'package:artifact/pages/programs/ProgramsPage.dart';
import 'package:artifact/widgets/Heading2Text.dart';
import 'package:artifact/widgets/Heading3Text.dart';
import 'package:artifact/widgets/ParagraphText.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class ProgramsState extends State<ProgramsPage> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return new Scaffold(
        body: new Center(
            child: expandableList()
        ));
  }

  Widget expandableList() {
    return ListView.builder(
        itemCount: widget.programsDetails.length,
        itemBuilder: (context, i) {
          return new ExpansionTile(
            title: new Text(widget.programsDetails[i].title),
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new ListTile(subtitle: new Text(widget.programsDetails[i].description))
                ],
              ),
            ],
          );
        });
  }
}
