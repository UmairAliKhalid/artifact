/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/utils/StyleConstants.dart';
import 'package:flutter/material.dart';

class NormalText extends TextStyle {

  NormalText({double fontSize = StyleConstants.TEXT})
      : super(
            fontFamily: 'RobotoMono',
            fontWeight: FontWeight.normal,
            fontSize: fontSize);
}
