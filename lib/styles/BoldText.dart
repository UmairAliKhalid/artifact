/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/utils/StyleConstants.dart';
import 'package:flutter/widgets.dart';

abstract class BoldText extends TextStyle {

  BoldText({double fontSize = StyleConstants.TEXT})
      : super(
            fontFamily: 'RobotoMono',
            fontWeight: FontWeight.bold,
            fontSize: fontSize);
}
