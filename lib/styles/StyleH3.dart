/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/BoldText.dart';
import 'package:artifact/styles/StyleH1.dart';
import 'package:artifact/utils/StyleConstants.dart';

class StyleH3 extends StyleH1 {
  StyleH3() : super(fontSize : StyleConstants.H3);
}
