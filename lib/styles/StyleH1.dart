/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/BoldText.dart';
import 'package:artifact/utils/StyleConstants.dart';

class StyleH1 extends BoldText {
  StyleH1({double fontSize = StyleConstants.H1}) : super(fontSize : fontSize);
}
