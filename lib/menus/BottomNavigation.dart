import 'package:flutter/material.dart';

class BottomNavigation extends BottomNavigationBar {

  List<BottomNavigationBarItem> mItems;

  BottomNavigation(List<BottomNavigationBarItem> items) : super(items: items);
}