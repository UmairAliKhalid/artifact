/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/StyleH2.dart';
import 'package:artifact/styles/StyleH3.dart';
import 'package:flutter/widgets.dart';

class Heading3Text extends Text {
  Heading3Text(String data) : super(data, style: StyleH3());
}
