/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/StyleH1.dart';
import 'package:flutter/widgets.dart';

class Heading1Text extends Text {
  Heading1Text(String data) : super(data, style: StyleH1());
}
