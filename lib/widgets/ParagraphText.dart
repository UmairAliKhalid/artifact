/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/StyleParagraph.dart';
import 'package:flutter/widgets.dart';

class ParagraphText extends Text {
  ParagraphText(String data) : super(data, style: new StyleParagraph());
}
