/*
// Created by UMAIR KHALID on 1/9/2020.
*/

import 'package:artifact/styles/StyleH2.dart';
import 'package:flutter/widgets.dart';

class Heading2Text extends Text {
  Heading2Text(String data) : super(data, style: StyleH2());
}
