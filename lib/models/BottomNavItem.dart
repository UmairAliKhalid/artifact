/*
// Created by UMAIR KHALID on 1/13/2020.
*/

import 'package:flutter/material.dart';

class BottomNavItem {
  String title;
  IconData icon;

  //Constructor
  BottomNavItem(this.title, this.icon);
}

