/*
// Created by UMAIR KHALID on 1/13/2020.
*/

import 'package:flutter/widgets.dart';

class DrawerItem {
  String title;
  IconData icon;

  //Constructor
  DrawerItem(this.title, this.icon);
}
