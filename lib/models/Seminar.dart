/*
// Created by UMAIR KHALID on 1/14/2020.
*/

class Seminar {

  String name;
  String speaker;
  String link;

  Seminar(this.name, this.speaker, this.link);
}
