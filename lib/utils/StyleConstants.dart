/*
// Created by UMAIR KHALID on 1/9/2020.
*/

class StyleConstants {

  static const double H1 = 30;
  static const double H2 = 25;
  static const double H3 = 20;
  static const double H4 = 15;

  static const double LARGE_TEXT = 14;
  static const double TEXT = 12;
  static const double MINI_TEXT = 10;

  static const double MATERIAL_PADDING = 16;
  static const double MATERIAL_PADDING_SCREEN = 25;

}
