/*
// Created by UMAIR KHALID on 1/6/2020.
*/

import 'package:logger/logger.dart';

class Log {

  final String appName = "Artifact";
  final Logger logger = new Logger();

  static const IS_ENABLE = true;
  static final Log _log = Log._internal();

  factory Log(){
    return _log;
  }

  Log._internal();


  e(String error) {
    if (IS_ENABLE) logger.e(appName, error);
  }

  d(String debugg) {
    if (IS_ENABLE) logger.d(appName, debugg);
  }

  i(String info) {
    if (IS_ENABLE) logger.i(appName, info);
  }

  v(String verbose) {
    if (IS_ENABLE) logger.v(appName, verbose);
  }

  w(String warning) {
    if (IS_ENABLE) logger.w(appName, warning);
  }

  wtf(String wtf) {
    if (IS_ENABLE) logger.wtf(appName, wtf);
  }
}

